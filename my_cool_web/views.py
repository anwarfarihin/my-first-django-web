from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse

from .models import Schedule
from . import forms


# Create your views here.
def index(request):
    return render(request,"index.html")

def aboutme(request):
    return render(request,"aboutme.html")

def guestbook(request):
    return render(request,"guestbook.html")

def skills(request):
    return render(request,"skills.html")

def personalinfos(request):
    return render(request,"personalinfos.html")

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})


def add_schedule(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'addSchedule.html', {'form': form})

def resetAll(request):
    Schedule.objects.all().delete()
    schedules =[]
    return render(request, 'schedule.html', {'schedules': schedules})