from django.conf.urls import url
from . import views
from django.urls import path

urlpatterns = [
    path('', views.index, name='index'),
    path('index/', views.index, name='index'),
    path('aboutme/', views.aboutme, name = "aboutme"),
    path('personalinfos/', views.personalinfos, name = "personalinfos"),
    path('skills/', views.skills, name = "skills"),
    path('guestbook/', views.guestbook, name = "guestbook"),
    path('schedule/', views.schedule, name = "schedule"),
    path('schedule/add', views.add_schedule, name = "add_schedule"),
    path('resetAll/',views.resetAll,name = "resetAll"),
]